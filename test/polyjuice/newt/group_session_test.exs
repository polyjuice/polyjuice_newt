# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.GroupSessionTest do
  use ExUnit.Case

  test "encrypt and decrypt" do
    session = Polyjuice.Newt.GroupSession.create()
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 0
    key = Polyjuice.Newt.GroupSession.get_key(session)
    ciphertext = Polyjuice.Newt.GroupSession.encrypt(session, "Secret")

    {:ok, inbound} = Polyjuice.Newt.InboundGroupSession.create(key)

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(inbound)

    {:ok, {plaintext, index}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound, ciphertext)
    assert index == 0
    assert plaintext == "Secret"
  end

  test "can (re-)decrypt present and future messages but not past ones" do
    session = Polyjuice.Newt.GroupSession.create()
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 0
    key0 = Polyjuice.Newt.GroupSession.get_key(session)
    plaintext0 = "message 0"
    ciphertext0 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext0)
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 1
    key1 = Polyjuice.Newt.GroupSession.get_key(session)
    plaintext1 = "message 1"
    ciphertext1 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext1)
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 2
    key2 = Polyjuice.Newt.GroupSession.get_key(session)
    plaintext2 = "message 2"
    ciphertext2 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext2)

    {:ok, inbound0} = Polyjuice.Newt.InboundGroupSession.create(key0)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(inbound0) == 0
    {:ok, inbound1} = Polyjuice.Newt.InboundGroupSession.create(key1)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(inbound1) == 1
    {:ok, inbound2} = Polyjuice.Newt.InboundGroupSession.create(key2)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(inbound2) == 2

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(inbound0)

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(inbound1)

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(inbound2)

    {:ok, {^plaintext0, 0}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound0, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(inbound1, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(inbound2, ciphertext0)

    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound0, ciphertext1)
    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound1, ciphertext1)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(inbound2, ciphertext1)

    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound0, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound1, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound2, ciphertext2)

    export0_0 = Polyjuice.Newt.InboundGroupSession.export_at(inbound0, 0)
    export0_1 = Polyjuice.Newt.InboundGroupSession.export_at(inbound0, 1)
    export0_2 = Polyjuice.Newt.InboundGroupSession.export_at(inbound0, 2)

    nil = Polyjuice.Newt.InboundGroupSession.export_at(inbound1, 0)
    export1_1 = Polyjuice.Newt.InboundGroupSession.export_at(inbound1, 1)
    export1_2 = Polyjuice.Newt.InboundGroupSession.export_at(inbound2, 2)

    nil = Polyjuice.Newt.InboundGroupSession.export_at(inbound2, 0)
    nil = Polyjuice.Newt.InboundGroupSession.export_at(inbound2, 1)
    export2_2 = Polyjuice.Newt.InboundGroupSession.export_at(inbound2, 2)

    {:ok, import0_0} = Polyjuice.Newt.InboundGroupSession.import_key(export0_0)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import0_0) == 0

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import0_0)

    {:ok, import0_1} = Polyjuice.Newt.InboundGroupSession.import_key(export0_1)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import0_1) == 1

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import0_1)

    {:ok, import0_2} = Polyjuice.Newt.InboundGroupSession.import_key(export0_2)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import0_2) == 2

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import0_2)

    {:ok, import1_1} = Polyjuice.Newt.InboundGroupSession.import_key(export1_1)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import1_1) == 1

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import1_1)

    export1_1 = Polyjuice.Newt.InboundGroupSession.export_at_first_known_index(inbound1)

    {:ok, import1_1} = Polyjuice.Newt.InboundGroupSession.import_key(export1_1)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import1_1) == 1

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import1_1)

    {:ok, import1_2} = Polyjuice.Newt.InboundGroupSession.import_key(export1_2)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import1_2) == 2

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import1_2)

    {:ok, import2_2} = Polyjuice.Newt.InboundGroupSession.import_key(export2_2)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(import2_2) == 2

    assert Polyjuice.Newt.GroupSession.get_id(session) ==
             Polyjuice.Newt.InboundGroupSession.get_id(import2_2)

    {:ok, {^plaintext0, 0}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_0, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import0_1, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import0_2, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import1_1, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import1_2, ciphertext0)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import2_2, ciphertext0)

    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_0, ciphertext1)
    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_1, ciphertext1)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import0_2, ciphertext1)

    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(import1_1, ciphertext1)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import1_2, ciphertext1)

    {:error, :unknown_message_index} =
      Polyjuice.Newt.InboundGroupSession.decrypt(import2_2, ciphertext1)

    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_0, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_1, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import0_2, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import1_1, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import1_2, ciphertext2)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(import2_2, ciphertext2)
  end

  test "can compare inbound sessions" do
    session1 = Polyjuice.Newt.GroupSession.create()
    session2 = Polyjuice.Newt.GroupSession.create()

    key1 = Polyjuice.Newt.GroupSession.get_key(session1)
    key2 = Polyjuice.Newt.GroupSession.get_key(session2)

    {:ok, inbound1_1} = Polyjuice.Newt.InboundGroupSession.create(key1)
    {:ok, inbound1_2} = Polyjuice.Newt.InboundGroupSession.create(key1)
    {:ok, inbound2} = Polyjuice.Newt.InboundGroupSession.create(key2)

    assert Polyjuice.Newt.InboundGroupSession.connected?(inbound1_1, inbound1_2)
    assert Polyjuice.Newt.InboundGroupSession.compare(inbound1_1, inbound1_2) == :equal
    assert not Polyjuice.Newt.InboundGroupSession.connected?(inbound1_1, inbound2)
    assert Polyjuice.Newt.InboundGroupSession.compare(inbound1_1, inbound2) == :unconnected
    assert not Polyjuice.Newt.InboundGroupSession.connected?(inbound2, inbound1_1)
    assert Polyjuice.Newt.InboundGroupSession.compare(inbound2, inbound1_1) == :unconnected

    assert Polyjuice.Newt.InboundGroupSession.advance_to(inbound1_2, 5)
    assert not Polyjuice.Newt.InboundGroupSession.advance_to(inbound1_2, 4)

    assert Polyjuice.Newt.InboundGroupSession.compare(inbound1_1, inbound1_2) == :better
    assert Polyjuice.Newt.InboundGroupSession.compare(inbound1_2, inbound1_1) == :worse
  end

  test "can pickle and unpickle" do
    session = Polyjuice.Newt.GroupSession.create()

    key0 = Polyjuice.Newt.GroupSession.get_key(session)
    plaintext0 = "message 0"
    ciphertext0 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext0)
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 1

    key = "12345678901234567890123456789012"
    {:ok, session_pickle} = Polyjuice.Newt.GroupSession.pickle(session, key)
    {:ok, session} = Polyjuice.Newt.GroupSession.from_pickle(session_pickle, key)

    plaintext1 = "message 1"
    ciphertext1 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext1)
    assert Polyjuice.Newt.GroupSession.get_message_index(session) == 2
    plaintext2 = "message 2"
    ciphertext2 = Polyjuice.Newt.GroupSession.encrypt(session, plaintext2)

    {:ok, inbound} = Polyjuice.Newt.InboundGroupSession.create(key0)
    assert Polyjuice.Newt.InboundGroupSession.first_known_index(inbound) == 0
    {:ok, {^plaintext0, 0}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound, ciphertext0)
    {:ok, {^plaintext1, 1}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound, ciphertext1)

    {:ok, inbound_pickle} = Polyjuice.Newt.InboundGroupSession.pickle(inbound, key)
    {:ok, inbound} = Polyjuice.Newt.InboundGroupSession.from_pickle(inbound_pickle, key)
    {:ok, {^plaintext2, 2}} = Polyjuice.Newt.InboundGroupSession.decrypt(inbound, ciphertext2)
  end
end
