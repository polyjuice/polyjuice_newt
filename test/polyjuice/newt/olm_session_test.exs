# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.OlmSessionTest do
  use ExUnit.Case

  test "create session" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    plaintext1 = "message 1"
    {0, message} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext1)

    {:ok, {bob_session, decrypted}} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message
      )

    assert Polyjuice.Newt.OlmSession.get_id(alice_session) ==
             Polyjuice.Newt.OlmSession.get_id(bob_session)

    assert decrypted == plaintext1

    plaintext2 = "message 2"
    {0, message} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext2)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message)
    assert decrypted == plaintext2

    plaintext3 = "message 3"
    {1, message} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext3)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message)
    assert decrypted == plaintext3

    plaintext4 = "message 4"
    {1, message} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext4)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message)
    assert decrypted == plaintext4

    plaintext5 = "message 5"
    {1, message} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext5)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message)
    assert decrypted == plaintext5
  end

  test "decrypts out of order" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    plaintext1 = "message 1"
    {0, message1} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext1)
    plaintext2 = "message 2"
    {0, message2} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext2)

    {:ok, {bob_session, decrypted}} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message2
      )

    assert Polyjuice.Newt.OlmSession.get_id(alice_session) ==
             Polyjuice.Newt.OlmSession.get_id(bob_session)

    assert decrypted == plaintext2

    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message1)
    assert decrypted == plaintext1

    plaintext3 = "message 3"
    {1, message3} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext3)
    plaintext4 = "message 4"
    {1, message4} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext4)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message4)
    assert decrypted == plaintext4
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message3)
    assert decrypted == plaintext3

    plaintext5 = "message 5"
    {1, message5} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext5)
    plaintext6 = "message 6"
    {1, message6} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext6)
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message6)
    assert decrypted == plaintext6
    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message5)
    assert decrypted == plaintext5
  end

  test "can't decrypt twice" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    plaintext1 = "message 1"
    {0, message1} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext1)

    {:ok, {bob_session, _}} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message1
      )

    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message1)

    plaintext2 = "message 2"
    {0, message2} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext2)
    {:ok, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message2)
    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message2)

    plaintext3 = "message 3"
    {1, message3} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext3)
    {:ok, _} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message3)
    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 0, message3)

    plaintext4 = "message 4"
    {1, message4} = Polyjuice.Newt.OlmSession.encrypt(bob_session, plaintext4)
    {:ok, _} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message4)
    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(alice_session, 1, message4)

    plaintext5 = "message 5"
    {1, message5} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext5)
    {:ok, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message5)
    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message5)

    plaintext6 = "message 6"
    {1, message6} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext6)
    {:ok, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message6)
    {:error, _} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 1, message6)
  end

  test "can pickle and unpickle" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    plaintext1 = "message 1"
    {0, message1} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext1)
    plaintext2 = "message 2"
    {0, message2} = Polyjuice.Newt.OlmSession.encrypt(alice_session, plaintext2)

    {:ok, {bob_session, _}} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message1
      )

    key = "12345678901234567890123456789012"
    {:ok, bob_session_pickle} = Polyjuice.Newt.OlmSession.pickle(bob_session, key)
    {:ok, bob_session} = Polyjuice.Newt.OlmSession.from_pickle(bob_session_pickle, key)

    {:ok, decrypted} = Polyjuice.Newt.OlmSession.decrypt(bob_session, 0, message2)
    assert decrypted == plaintext2

    # using the wrong key length should result in an error
    assert Polyjuice.Newt.OlmSession.pickle(bob_session, "123") == {:error, :invalid_key_length}

    # using the wrong key should result in an error
    assert Polyjuice.Newt.OlmSession.from_pickle(
             bob_session_pickle,
             "09876543210987654321098765432109"
           ) == {:error, :decryption}
  end
end
