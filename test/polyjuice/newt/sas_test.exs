# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.SasTest do
  use ExUnit.Case

  test "sas" do
    {alice_sas, alice_key} = Polyjuice.Newt.Sas.create()
    {bob_sas, bob_key} = Polyjuice.Newt.Sas.create()

    {:ok, alice_established_sas} = Polyjuice.Newt.Sas.establish(alice_sas, bob_key)
    {:ok, bob_established_sas} = Polyjuice.Newt.Sas.establish(bob_sas, alice_key)

    auth_string = Polyjuice.Newt.Sas.generate_auth_string(alice_established_sas, "info")
    assert %{emoji: {_, _, _, _, _, _, _}, decimal: {_, _, _}} = auth_string

    assert Polyjuice.Newt.Sas.generate_auth_string(alice_established_sas, "info") ==
             Polyjuice.Newt.Sas.generate_auth_string(bob_established_sas, "info")

    assert Polyjuice.Newt.Sas.generate_auth_string(alice_established_sas, "info") !=
             Polyjuice.Newt.Sas.generate_auth_string(bob_established_sas, "info2")

    mac = Polyjuice.Newt.Sas.calculate_mac(alice_established_sas, "input", "info")

    assert Polyjuice.Newt.Sas.verify_mac(bob_established_sas, "input", "info", mac) == {:ok, {}}

    assert Polyjuice.Newt.Sas.verify_mac(bob_established_sas, "input", "info2", mac) ==
             {:error, :invalid_mac}
  end

  test "sas with invalid base64" do
    {alice_sas, alice_key} = Polyjuice.Newt.Sas.create()
    {bob_sas, bob_key} = Polyjuice.Newt.Sas.create()

    {:ok, alice_established_sas} = Polyjuice.Newt.Sas.establish(alice_sas, bob_key)
    {:ok, bob_established_sas} = Polyjuice.Newt.Sas.establish(bob_sas, alice_key)

    assert Polyjuice.Newt.Sas.generate_auth_string(alice_established_sas, "info") ==
             Polyjuice.Newt.Sas.generate_auth_string(bob_established_sas, "info")

    assert Polyjuice.Newt.Sas.generate_auth_string(alice_established_sas, "info") !=
             Polyjuice.Newt.Sas.generate_auth_string(bob_established_sas, "info2")

    mac = Polyjuice.Newt.Sas.calculate_mac(alice_established_sas, "input", "info")

    alice_mac_invalid_base64 =
      Polyjuice.Newt.Sas.calculate_mac_invalid_base64(alice_established_sas, "input", "info")

    assert mac != alice_mac_invalid_base64

    bob_mac_invalid_base64 =
      Polyjuice.Newt.Sas.calculate_mac_invalid_base64(bob_established_sas, "input", "info")

    assert alice_mac_invalid_base64 == bob_mac_invalid_base64
  end

  test "sas index" do
    assert Polyjuice.Newt.Sas.emoji_for_index(0) == {"🐶", "Dog"}
    assert Polyjuice.Newt.Sas.emoji_for_index(64) == nil
  end
end
