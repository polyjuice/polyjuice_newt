# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.AccountTest do
  use ExUnit.Case

  test "sign" do
    account = Polyjuice.Newt.Account.create()

    signature = Polyjuice.Newt.Account.sign(account, "foo")

    {:ok, raw_key} = Base.decode64(Polyjuice.Newt.Account.ed25519_key(account), padding: false)
    {:ok, raw_sig} = Base.decode64(signature, padding: false)
    assert :crypto.verify(:eddsa, :none, "foo", raw_sig, [raw_key, :ed25519]) == true
  end

  test "keys get marked as published" do
    account = Polyjuice.Newt.Account.create()

    num = Polyjuice.Newt.Account.max_number_of_one_time_keys(account)

    Polyjuice.Newt.Account.generate_one_time_keys(account, num)
    Polyjuice.Newt.Account.generate_fallback_key(account)

    assert Enum.count(Polyjuice.Newt.Account.get_one_time_keys(account)) == num
    assert Enum.count(Polyjuice.Newt.Account.get_fallback_key(account)) == 1

    Polyjuice.Newt.Account.mark_keys_as_published(account)

    assert Polyjuice.Newt.Account.get_one_time_keys(account) == %{}
    assert Polyjuice.Newt.Account.get_fallback_key(account) == %{}
  end

  test "one-time keys can be used only once" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session_1} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_1} = Polyjuice.Newt.OlmSession.encrypt(alice_session_1, "Hi")

    {:ok, alice_session_2} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_2} = Polyjuice.Newt.OlmSession.encrypt(alice_session_2, "Hi")

    {:ok, _} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_1
      )

    # FIXME: error code
    {:error, _} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_2
      )
  end

  test "fallback keys can be used multiple times until we forget them" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_fallback_key(bob)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_fallback_key(bob) |> Enum.take(1)

    {:ok, alice_session_1} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_1} = Polyjuice.Newt.OlmSession.encrypt(alice_session_1, "Hi")

    {:ok, alice_session_2} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_2} = Polyjuice.Newt.OlmSession.encrypt(alice_session_2, "Hi")

    {:ok, alice_session_3} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_3} = Polyjuice.Newt.OlmSession.encrypt(alice_session_3, "Hi")

    {:ok, _} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_1
      )

    Polyjuice.Newt.Account.generate_fallback_key(bob)

    {:ok, _} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_2
      )

    true = Polyjuice.Newt.Account.forget_fallback_key(bob)

    # FIXME: error code
    {:error, _} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_3
      )
  end

  test "can pickle and unpickle" do
    alice = Polyjuice.Newt.Account.create()
    bob = Polyjuice.Newt.Account.create()

    Polyjuice.Newt.Account.generate_one_time_keys(bob, 1)
    [{_key_id, key}] = Polyjuice.Newt.Account.get_one_time_keys(bob) |> Enum.take(1)

    {:ok, alice_session} =
      Polyjuice.Newt.OlmSession.create_outbound(
        alice,
        Polyjuice.Newt.Account.curve25519_key(bob),
        key
      )

    {0, message_1} = Polyjuice.Newt.OlmSession.encrypt(alice_session, "Hi")

    key = "12345678901234567890123456789012"
    {:ok, bob_pickle} = Polyjuice.Newt.Account.pickle(bob, key)
    {:ok, bob} = Polyjuice.Newt.Account.from_pickle(bob_pickle, key)

    {:ok, {_, "Hi"}} =
      Polyjuice.Newt.OlmSession.create_inbound(
        bob,
        Polyjuice.Newt.Account.curve25519_key(alice),
        message_1
      )

    # using the wrong key length should result in an error
    assert Polyjuice.Newt.Account.pickle(bob, "123") == {:error, :invalid_key_length}

    # using the wrong key should result in an error
    assert Polyjuice.Newt.Account.from_pickle(bob_pickle, "09876543210987654321098765432109") ==
             {:error, :decryption}
  end
end
