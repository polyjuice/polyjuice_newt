# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.Account.SigningKeyTest do
  use ExUnit.Case

  test "signing JSON" do
    account = Polyjuice.Newt.Account.create()

    {:ok, signed_json} =
      Polyjuice.Util.JSON.sign(
        %{"one" => 1, "two" => "Two"},
        "@user:example.org",
        Polyjuice.Newt.Account.SigningKey.from_account(account, "ADEVICE")
      )

    assert Map.fetch!(signed_json, "signatures")
           |> Map.fetch!("@user:example.org")
           |> Map.fetch!("ed25519:ADEVICE")
           |> is_binary()

    assert Polyjuice.Util.JSON.signed?(
             signed_json,
             "@user:example.org",
             Polyjuice.Util.Ed25519.VerifyKey.from_base64(
               Polyjuice.Newt.Account.ed25519_key(account),
               "ADEVICE"
             )
           ) == true
  end
end
