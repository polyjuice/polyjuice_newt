# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.InboundGroupSession do
  @moduledoc """
  Functions related to inbound Megolm sessions.

  Inbound sessions are used for decrypting messages.
  """

  alias Polyjuice.Newt.Nif

  @typedoc """
  Errors that can be emitted when decoding a session key.
  """
  @type session_key_decode_error() ::
          :invalid_version
          | :invalid_key_length
          | :invalid_base64
          | :invalid_signature
          | Polyjuice.Newt.key_error()

  @typedoc """
  Errors that can be emitted when decrypting Megolm messages.
  """
  @type decryption_error() ::
          :invalid_signature
          | :invalid_mac
          | :invalid_padding
          | :unknown_message_index
          | Polyjuice.Newt.decode_error()

  @type t() :: reference

  @doc """
  Create an inbound Megolm session from the key obtained from
  `Polyjuice.Newt.GroupSession.get_key/1`.
  """
  @spec create(String.t()) :: {:ok, t()} | {:error, session_key_decode_error()}
  defdelegate create(key), to: Nif, as: :inbound_group_session_create

  @doc """
  Create an inbound Megolm session from the export-format key obtained from
  `Polyjuice.Newt.InboundGroupSession.export_at/2`.
  """
  @spec import_key(String.t()) :: {:ok, t()} | {:error, session_key_decode_error()}
  defdelegate import_key(key), to: Nif, as: :inbound_group_session_import

  @doc """
  Get the Megolm session ID.
  """
  @spec get_id(t()) :: String.t()
  defdelegate get_id(session), to: Nif, as: :inbound_group_session_id

  @doc """
  Determines if two Megolm sessions are connected.

  Two Megolm sessions are connected if they have the same session ID and are
  based on the same ratchet.
  """
  @spec connected?(t(), t()) :: boolean()
  defdelegate connected?(session1, session2), to: Nif, as: :inbound_group_session_connected

  @doc """
  Compares the order of two Megolm sessions.

  Determines which session comes before the other, if the sessions are connected.
  """
  @spec compare(t(), t()) :: :equal | :better | :worse | :unconnected
  defdelegate compare(session1, session2), to: Nif, as: :inbound_group_session_compare

  @doc """
  Gets the first known index.
  """
  @spec first_known_index(t()) :: non_neg_integer()
  defdelegate first_known_index(session), to: Nif, as: :inbound_group_session_first_known_index

  @doc """
  Advances the ratchet do an index.

  Returns `true` if the ratchet was advanced to the index, and `false` if the
  session was already advanced past the index.  After advancing the ratchet,
  the session will not be able to decrypt messages encrypted using previous
  indexes.
  """
  @spec advance_to(t(), non_neg_integer()) :: boolean()
  defdelegate advance_to(session, index), to: Nif, as: :inbound_group_session_advance_to

  @doc """
  Decrypts a message.  Returns the plaintext and the message index.
  """
  @spec decrypt(t(), String.t()) ::
          {:ok, {binary(), non_neg_integer()}}
          | {:error, Polyjuice.Newt.decode_error() | decryption_error()}
  defdelegate decrypt(session, ciphertext), to: Nif, as: :inbound_group_session_decrypt

  @doc """
  Exports the session key at the given index.

  Returns `nil` if the index is before the first known index.
  """
  @spec export_at(t(), non_neg_integer()) :: String.t() | nil
  defdelegate export_at(sesion, index), to: Nif, as: :inbound_group_session_export_at

  @doc """
  Exports the session at the first known index.
  """
  @spec export_at_first_known_index(t()) :: String.t()
  defdelegate export_at_first_known_index(session),
    to: Nif,
    as: :inbound_group_session_export_at_first_known_index

  @doc """
  Pickles (serializes and encrypts) the account for storage.

  The key must be 32 bytes long.
  """
  @spec pickle(t(), binary()) :: {:ok, String.t()} | {:error, :invalid_key_length}
  defdelegate pickle(account, key), to: Nif, as: :inbound_group_session_pickle

  @doc """
  Creates an account from a pickle.

  The key must be 32 bytes long.
  """
  @spec from_pickle(String.t(), binary()) :: {:ok, t()} | {:error, Polyjuice.Newt.pickle_error()}
  defdelegate from_pickle(pickle, key), to: Nif, as: :inbound_group_session_from_pickle

  @doc """
  Creates an account from a pickle created by libolm.
  """
  @spec from_libolm_pickle(String.t(), binary()) ::
          {:ok, t()} | {:error, Polyjuice.Newt.libolm_pickle_error()}
  defdelegate from_libolm_pickle(pickle, key),
    to: Nif,
    as: :inbound_group_session_from_libolm_pickle
end
