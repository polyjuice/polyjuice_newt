# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.Sas do
  @moduledoc """
  Functions related to Short Authentication String (SAS) verification.
  """

  alias Polyjuice.Newt.Nif

  @typedoc """
  An SAS session.
  """
  @type t() :: reference

  @typedoc """
  An established SAS shared secret.
  """
  @type established_sas() :: reference

  @typedoc """
  Errors that can be emitted when verifying a MAC
  """
  @type verify_error() :: :invalid_base64 | :invalid_mac

  @typedoc """
  Emoji indices to be use for SAS in emoji form.

  A tuple of 7 integers, which can be converted to the emoji and description by
  using `emoji_for_index/2`.
  """
  @type emoji_sas() ::
          {non_neg_integer, non_neg_integer, non_neg_integer, non_neg_integer, non_neg_integer,
           non_neg_integer, non_neg_integer}

  @typedoc """
  SAS in decimal form.
  """
  @type decimal_sas() :: {non_neg_integer, non_neg_integer, non_neg_integer}

  @doc """
  Creates a new SAS object, and return its public key.
  """
  @spec create() :: {t(), String.t()}
  defdelegate create(), to: Nif, as: :sas_create

  @doc """
  Establish an SAS shared secret, using the other user's public key.

  This function can only be called once on a SAS object; calling it multiple
  times will result in an error.
  """
  @spec establish(t(), String.t()) ::
          {:ok, established_sas()} | {:error, Polyjuice.Newt.key_error()}
  defdelegate establish(sas, info), to: Nif, as: :sas_establish

  @doc """
  Generate the authentication strings from the established shared secret.

  The `info` parameter is created as described at
  https://spec.matrix.org/unstable/client-server-api/#hkdf-calculation

  Returns two tuples.  The first tuple (7 numbers between 0 and 63 inclusive)
  is the emoji indices and the second tuple (3 4-digit numbers between 1000 and
  9191 inclusive) is the decimal codes.

  """
  @spec generate_auth_string(established_sas(), String.t()) ::
          %{emoji: emoji_sas(), decimal: decimal_sas()}
  defdelegate generate_auth_string(established_sas, info), to: Nif, as: :sas_generate_auth_string

  @doc """
  Calculate the MAC of a key.

  The `info` parameter is created as described at
  https://spec.matrix.org/unstable/client-server-api/#hkdf-calculation and the
  `input` is the key being MAC'ed
  """
  @spec calculate_mac(established_sas(), String.t(), String.t()) :: String.t()
  defdelegate calculate_mac(established_sas, input, info), to: Nif, as: :sas_calculate_mac

  @doc """
  Calculate the MAC of a key.

  This function is provided for compatibility with libolm.

  The `info` parameter is created as described at
  https://spec.matrix.org/unstable/client-server-api/#hkdf-calculation and the
  `input` is the key being MAC'ed
  """
  @spec calculate_mac_invalid_base64(established_sas(), String.t(), String.t()) :: String.t()
  defdelegate calculate_mac_invalid_base64(established_sas, input, info),
    to: Nif,
    as: :sas_calculate_mac_invalid_base64

  @doc """
  Verifies the MAC of a key.
  """
  @spec verify_mac(established_sas(), String.t(), String.t(), String.t()) ::
          {:ok, {}} | {:error, verify_error()}
  defdelegate verify_mac(established_sas, input, info, tag), to: Nif, as: :sas_verify_mac

  @doc """
  Returns our public key used to establish the SAS.
  """
  @spec our_public_key(established_sas()) :: String.t()
  defdelegate our_public_key(established_sas), to: Nif, as: :sas_our_public_key

  @doc """
  Returns their public key used to establish the SAS.
  """
  @spec their_public_key(established_sas()) :: String.t()
  defdelegate their_public_key(established_sas), to: Nif, as: :sas_their_public_key

  @emoji %{
    0 => {"🐶", "Dog"},
    1 => {"🐱", "Cat"},
    2 => {"🦁", "Lion"},
    3 => {"🐎", "Horse"},
    4 => {"🦄", "Unicorn"},
    5 => {"🐷", "Pig"},
    6 => {"🐘", "Elephant"},
    7 => {"🐰", "Rabbit"},
    8 => {"🐼", "Panda"},
    9 => {"🐓", "Rooster"},
    10 => {"🐧", "Penguin"},
    11 => {"🐢", "Turtle"},
    12 => {"🐟", "Fish"},
    13 => {"🐙", "Octopus"},
    14 => {"🦋", "Butterfly"},
    15 => {"🌷", "Flower"},
    16 => {"🌳", "Tree"},
    17 => {"🌵", "Cactus"},
    18 => {"🍄", "Mushroom"},
    19 => {"🌏", "Globe"},
    20 => {"🌙", "Moon"},
    21 => {"☁️", "Cloud"},
    22 => {"🔥", "Fire"},
    23 => {"🍌", "Banana"},
    24 => {"🍎", "Apple"},
    25 => {"🍓", "Strawberry"},
    26 => {"🌽", "Corn"},
    27 => {"🍕", "Pizza"},
    28 => {"🎂", "Cake"},
    29 => {"❤️", "Heart"},
    30 => {"😀", "Smiley"},
    31 => {"🤖", "Robot"},
    32 => {"🎩", "Hat"},
    33 => {"👓", "Glasses"},
    34 => {"🔧", "Spanner"},
    35 => {"🎅", "Santa"},
    36 => {"👍", "Thumbs Up"},
    37 => {"☂️", "Umbrella"},
    38 => {"⌛", "Hourglass"},
    39 => {"⏰", "Clock"},
    40 => {"🎁", "Gift"},
    41 => {"💡", "Light Bulb"},
    42 => {"📕", "Book"},
    43 => {"✏️", "Pencil"},
    44 => {"📎", "Paperclip"},
    45 => {"✂️", "Scissors"},
    46 => {"🔒", "Lock"},
    47 => {"🔑", "Key"},
    48 => {"🔨", "Hammer"},
    49 => {"☎️", "Telephone"},
    50 => {"🏁", "Flag"},
    51 => {"🚂", "Train"},
    52 => {"🚲", "Bicycle"},
    53 => {"✈️", "Aeroplane"},
    54 => {"🚀", "Rocket"},
    55 => {"🏆", "Trophy"},
    56 => {"⚽", "Ball"},
    57 => {"🎸", "Guitar"},
    58 => {"🎺", "Trumpet"},
    59 => {"🔔", "Bell"},
    60 => {"⚓", "Anchor"},
    61 => {"🎧", "Headphones"},
    62 => {"📁", "Folder"},
    63 => {"📌", "Pin"}
  }

  @doc """
  Returns the emoji and name for a given index.

  The name is the untranslated English name given in the spec.  Translations in
  various languages can be found at
  https://github.com/matrix-org/matrix-doc/tree/old_master/data-definitions

  """
  @spec emoji_for_index(non_neg_integer) :: {String.t(), String.t()} | nil
  def emoji_for_index(index) when is_integer(index), do: Map.get(@emoji, index)
end
