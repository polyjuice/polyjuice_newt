# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.Account do
  @moduledoc """
  Functions related to Olm accounts.
  """

  alias Polyjuice.Newt.Nif

  @type t() :: reference

  @doc """
  Creates a new account with random identity keys.
  """
  @spec create() :: t()
  defdelegate create(), to: Nif, as: :account_create

  @doc """
  Returns the account's ed25519 key.
  """
  @spec ed25519_key(t()) :: String.t()
  defdelegate ed25519_key(account), to: Nif, as: :account_ed25519_key

  @doc """
  Returns the account's curve25519 key.
  """
  @spec curve25519_key(t()) :: String.t()
  defdelegate curve25519_key(account), to: Nif, as: :account_curve25519_key

  @doc """
  Sign a message with the account's ed25519 key.
  """
  @spec sign(t(), String.t()) :: String.t()
  defdelegate sign(account, message), to: Nif, as: :account_sign

  @doc """
  Gets the maximum number of one-time keys that the account can keep track of.

  If more than this number of keys is generated (and not used to create inbound
  Olm sessions), then keys will be deleted, oldest first.
  """
  @spec max_number_of_one_time_keys(t()) :: non_neg_integer
  defdelegate max_number_of_one_time_keys(account),
    to: Nif,
    as: :account_max_number_of_one_time_keys

  @doc """
  Generates one-time keys.
  """
  @spec generate_one_time_keys(t(), non_neg_integer) :: nil
  defdelegate generate_one_time_keys(account, number),
    to: Nif,
    as: :account_generate_one_time_keys

  @doc """
  Retrieves any unpublished one-time keys.
  """
  @spec get_one_time_keys(t()) :: %{optional(String.t()) => String.t()}
  defdelegate get_one_time_keys(account), to: Nif, as: :account_get_one_time_keys

  @doc """
  Generates a fallback key.
  """
  @spec generate_fallback_key(t()) :: nil
  defdelegate generate_fallback_key(account), to: Nif, as: :account_generate_fallback_key

  @doc """
  Retrieves the unpublished fallback key (if any).
  """
  @spec get_fallback_key(t()) :: %{optional(String.t()) => String.t()}
  defdelegate get_fallback_key(account), to: Nif, as: :account_get_fallback_key

  @doc """
  Forgets the old fallback key (if any).
  """
  @spec forget_fallback_key(t()) :: bool
  defdelegate forget_fallback_key(account), to: Nif, as: :account_forget_fallback_key

  @doc """
  Marks the one-time keys and fallback key as published.

  After they are marked as published, they will no longer be returned by
  `get_one_time_keys/1` and `get_fallback_key/1`.
  """
  @spec mark_keys_as_published(t()) :: nil
  defdelegate mark_keys_as_published(account), to: Nif, as: :account_mark_keys_as_published

  @doc """
  Pickles (serializes and encrypts) the account for storage.

  The key must be 32 bytes long.
  """
  @spec pickle(t(), binary()) :: {:ok, String.t()} | {:error, :invalid_key_length}
  defdelegate pickle(account, key), to: Nif, as: :account_pickle

  @doc """
  Creates an account from a pickle.

  The key must be 32 bytes long.
  """
  @spec from_pickle(String.t(), binary()) :: {:ok, t()} | {:error, Polyjuice.Newt.pickle_error()}
  defdelegate from_pickle(pickle, key), to: Nif, as: :account_from_pickle

  @doc """
  Creates an account from a pickle created by libolm.
  """
  @spec from_libolm_pickle(String.t(), binary()) ::
          {:ok, t()} | {:error, Polyjuice.Newt.libolm_pickle_error()}
  defdelegate from_libolm_pickle(pickle, key), to: Nif, as: :account_from_libolm_pickle
end
