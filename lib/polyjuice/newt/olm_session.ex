# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.OlmSession do
  @moduledoc """
  Functions related to Olm sessions.
  """

  alias Polyjuice.Newt.Nif

  @type t() :: reference

  @typedoc """
  Errors that can be emitted when creating a session.
  """
  @type(
    session_creation_error() ::
      :invalid_base64
      | Polyjuice.Newt.decode_error()
      | :missing_one_time_key
      | :mismatched_identity_key,
    :decryption
  )

  @typedoc """
  Errors that can be emitted when decrypting olm messages.
  """
  @type decryption_error() ::
          :invalid_base64
          | :invalid_mac
          | :invalid_padding
          | :missing_message_key
          | :too_big_message_gap
          | Polyjuice.Newt.decode_error()

  @doc """
  Creates an outbound Olm session using the given device identity (curve25519)
  key and one-time key.
  """
  @spec create_outbound(Polyjuice.Newt.Account.t(), String.t(), String.t()) ::
          {:ok, t()} | {:error, Polyjuice.Newt.key_error()}
  defdelegate create_outbound(account, identity_key, one_time_key),
    to: Nif,
    as: :account_create_outbound_session

  @doc """
  Creates an inbound Olm session using the given device identity (curve25519)
  key and pre-key message.  A pre-key message is a message with
  `message_type = 0`.
  """
  @spec create_inbound(Polyjuice.Newt.Account.t(), String.t(), String.t()) ::
          {:ok, {t(), binary()}} | {:error, session_creation_error() | Polyjuice.Newt.key_error()}
  defdelegate create_inbound(account, identity_key, one_time_key),
    to: Nif,
    as: :account_create_inbound_session

  @doc """
  Returns the session's ID.
  """
  @spec get_id(t()) :: String.t()
  defdelegate get_id(session), to: Nif, as: :session_id

  @doc """
  Encrypts a message.  Returns a tuple of `{message_type, ciphertext}`, where
  `message_type` is 0 if it's a pre-key message, or 1 otherwise.
  """
  @spec encrypt(t(), binary()) :: {0 | 1, String.t()}
  defdelegate encrypt(session, message), to: Nif, as: :session_encrypt

  @doc """
  Returns the keys used to create the session.

  Returns the tuple `{identity_key, base_key, one_time_key}`
  """
  @spec get_keys(t()) :: {String.t(), String.t(), String.t()}
  defdelegate get_keys(session), to: Nif, as: :session_keys

  @doc """
  Decrypts a message.
  """
  @spec decrypt(t(), 0 | 1, String.t()) ::
          {:ok, binary()} | {:error, Polyjuice.Newt.decode_error() | decryption_error()}
  defdelegate decrypt(session, message_type, ciphertext), to: Nif, as: :session_decrypt

  @doc """
  Pickles (serializes and encrypts) the account for storage.

  The key must be 32 bytes long.
  """
  @spec pickle(t(), binary()) :: {:ok, String.t()} | {:error, :invalid_key_length}
  defdelegate pickle(account, key), to: Nif, as: :session_pickle

  @doc """
  Creates an account from a pickle.

  The key must be 32 bytes long.
  """
  @spec from_pickle(String.t(), binary()) :: {:ok, t()} | {:error, Polyjuice.Newt.pickle_error()}
  defdelegate from_pickle(pickle, key), to: Nif, as: :session_from_pickle

  @doc """
  Creates an account from a pickle created by libolm.
  """
  @spec from_libolm_pickle(String.t(), binary()) ::
          {:ok, t()} | {:error, Polyjuice.Newt.libolm_pickle_error()}
  defdelegate from_libolm_pickle(pickle, key), to: Nif, as: :session_from_libolm_pickle
end
