# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.Account.SigningKey do
  defstruct [
    :account,
    :id
  ]

  def from_account(account, id) when is_reference(account) and is_binary(id) do
    %__MODULE__{
      account: account,
      id: id
    }
  end

  defimpl Polyjuice.Util.SigningKey do
    def sign(%{account: account}, msg) do
      Polyjuice.Newt.Account.sign(account, msg)
    end

    def id(%{id: id}), do: "ed25519:" <> id
    def version(%{id: id}), do: id
    def algorithm(_), do: "ed25519"
  end
end
