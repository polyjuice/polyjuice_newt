# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.Nif do
  @moduledoc false
  use Rustler, otp_app: :polyjuice_newt, crate: "polyjuice_newt_nif"

  def account_create(), do: error()
  def account_ed25519_key(_account), do: error()
  def account_curve25519_key(_account), do: error()
  def account_sign(_account, _message), do: error()
  def account_max_number_of_one_time_keys(_account), do: error()
  def account_generate_one_time_keys(_account, _num_keys), do: error()
  def account_get_one_time_keys(_account), do: error()
  def account_generate_fallback_key(_account), do: error()
  def account_get_fallback_key(_account), do: error()
  def account_forget_fallback_key(_account), do: error()
  def account_mark_keys_as_published(_account), do: error()
  def account_create_outbound_session(_account, _identity_key, _one_time_key), do: error()
  def account_create_inbound_session(_account, _identity_key, _ciphertext), do: error()
  def account_pickle(_account, _key), do: error()
  def account_from_pickle(_pickle, _key), do: error()
  def account_from_libolm_pickle(_pickle, _key), do: error()

  def session_id(_session), do: error()
  def session_has_received_message(_session), do: error()
  def session_encrypt(_session, _plaintext), do: error()
  def session_keys(_session), do: error()
  def session_decrypt(_session, _message_type, _ciphertext), do: error()
  def session_pickle(_session, _key), do: error()
  def session_from_pickle(_pickle, _key), do: error()
  def session_from_libolm_pickle(_pickle, _key), do: error()

  def group_session_create(), do: error()
  def group_session_id(_session), do: error()
  def group_session_message_index(_session), do: error()
  def group_session_encrypt(_session, _message), do: error()
  def group_session_key(_session), do: error()
  def group_session_pickle(_session, _key), do: error()
  def group_session_from_pickle(_pickle, _key), do: error()
  def group_session_from_libolm_pickle(_pickle, _key), do: error()

  def inbound_group_session_create(_key), do: error()
  def inbound_group_session_import(_key), do: error()
  def inbound_group_session_id(_session), do: error()
  def inbound_group_session_connected(_session1, _session2), do: error()
  def inbound_group_session_compare(_session1, _session2), do: error()
  def inbound_group_session_first_known_index(_session), do: error()
  def inbound_group_session_advance_to(_session, _index), do: error()
  def inbound_group_session_decrypt(_session, _ciphertext), do: error()
  def inbound_group_session_export_at(_session, _index), do: error()
  def inbound_group_session_export_at_first_known_index(_session), do: error()
  def inbound_group_session_pickle(_session, _key), do: error()
  def inbound_group_session_from_pickle(_pickle, _key), do: error()
  def inbound_group_session_from_libolm_pickle(_pickle, _key), do: error()

  def sas_create(), do: error()
  def sas_establish(_sas, _key), do: error()
  def sas_generate_auth_string(_sas, _info), do: error()
  def sas_calculate_mac(_sas, _input, _info), do: error()
  def sas_calculate_mac_invalid_base64(_sas, _input, _info), do: error()
  def sas_verify_mac(_sas, _input, _info, _tag), do: error()
  def sas_our_public_key(_sas), do: error()
  def sas_their_public_key(_sas), do: error()

  defp error(), do: :erlang.nif_error(:nif_not_loaded)
end
