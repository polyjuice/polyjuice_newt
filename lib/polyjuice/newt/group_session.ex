# Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt.GroupSession do
  @moduledoc """
  Functions related to outbound Megolm sessions.

  Outbound sessions are used for encrypting messages.
  """

  alias Polyjuice.Newt.Nif

  @type t() :: reference

  @doc """
  Create a new outbound Megolm session.
  """
  @spec create() :: t()
  defdelegate create(), to: Nif, as: :group_session_create

  @doc """
  Get the Megolm session ID.
  """
  @spec get_id(t()) :: String.t()
  defdelegate get_id(session), to: Nif, as: :group_session_id

  @doc """
  Get the message index for the next message.
  """
  @spec get_message_index(t()) :: integer
  defdelegate get_message_index(session), to: Nif, as: :group_session_message_index

  @doc """
  Encrypt a message.
  """
  @spec encrypt(t(), binary()) :: String.t()
  defdelegate encrypt(session, message), to: Nif, as: :group_session_encrypt

  @doc """
  Get the session key for the next message.
  """
  @spec get_key(t()) :: String.t()
  defdelegate get_key(session), to: Nif, as: :group_session_key

  @doc """
  Pickles (serializes and encrypts) the account for storage.

  The key must be 32 bytes long.
  """
  @spec pickle(t(), binary()) :: {:ok, String.t()} | {:error, :invalid_key_length}
  defdelegate pickle(account, key), to: Nif, as: :group_session_pickle

  @doc """
  Creates an account from a pickle.

  The key must be 32 bytes long.
  """
  @spec from_pickle(String.t(), binary()) :: {:ok, t()} | {:error, Polyjuice.Newt.pickle_error()}
  defdelegate from_pickle(pickle, key), to: Nif, as: :group_session_from_pickle

  @doc """
  Creates an account from a pickle created by libolm.
  """
  @spec from_libolm_pickle(String.t(), binary()) ::
          {:ok, t()} | {:error, Polyjuice.Newt.libolm_pickle_error()}
  defdelegate from_libolm_pickle(pickle, key), to: Nif, as: :group_session_from_libolm_pickle
end
