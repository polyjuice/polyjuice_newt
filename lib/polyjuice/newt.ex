# Copyright 2022 The Matrix.org Foundation C.I.C.
# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Newt do
  @typedoc """
  Errors that can be emitted when using a key
  """
  @type key_error() ::
          :invalid_base64 | :invalid_key_length | :invalid_signature | :non_contributory_key

  @typedoc """
  Errors that can be emitted when decoding something
  """
  @type decode_error() ::
          :invalid_message_type
          | :missing_version
          | :message_too_short
          | :invalid_version
          | :invalid_mac
          | :invalid_signature
          | :proto_buf_error
          | :invalid_base64
          | key_error()

  @typedoc """
  Errors that can be emitted when unpickling
  """
  @type pickle_error() ::
          :invalid_base64
          | :decryption
          | :serialization

  @typedoc """
  Errors that can be emitted when unpickling from a libolm pickle
  """
  @type libolm_pickle_error() ::
          :invalid_base64
          | :missing_version
          | :invalid_version
          | :decryption
          | :invalid_public_key
          | :invalid_session
          | :decode
end
