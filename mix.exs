defmodule PolyjuiceNewt.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_newt,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "Polyjuice Newt",
      source_url: "https://gitlab.com/polyjuice/polyjuice_newt",
      homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/polyjuice/polyjuice_newt"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: 0],
        # the functions in Polyjuice.Newt.Nif never actually get called
        ignore_modules: [Polyjuice.Newt.Nif]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:polyjuice_util, "~> 0.2.0"},
      {:rustler, "~> 0.22.0"}
    ]
  end
end

# SPDX-FileCopyrightText: 2021 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0
