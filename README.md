# Polyjuice Newt

Newt is a binding for [vodozemac](https://github.com/matrix-org/vodozemac), a
Rust implementation olm and megolm.

## Installation

The package can be installed by adding `polyjuice_newt` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:polyjuice_newt, "~> 0.1.0"}
  ]
end
```

<!--
SPDX-FileCopyrightText: 2021 Hubert Chathi <hubert@uhoreg.ca>
SPDX-License-Identifier: CC0-1.0
-->
