/* Copyright 2021-2022 Hubert Chathi <hubert@uhoreg.ca>
 * Copyright 2022 The Matrix.org Foundation C.I.C.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#[macro_use]
extern crate rustler;

use rustler::resource::ResourceArc;
use rustler::{Atom, Binary, Encoder, Env, NifMap, OwnedBinary, Term};
use std::collections::HashMap;
use std::io::Write;
use std::sync::Mutex;
use vodozemac::megolm::{
    DecryptionError as MegolmDecryptionError, ExportedSessionKey, GroupSession, GroupSessionPickle,
    InboundGroupSession, InboundGroupSessionPickle, MegolmMessage,
    SessionConfig as MegolmSessionConfig, SessionKey, SessionKeyDecodeError, SessionOrdering,
};
use vodozemac::olm::{
    Account, AccountPickle, DecryptionError as OlmDecryptionError, OlmMessage, Session,
    SessionConfig as OlmSessionConfig, SessionCreationError, SessionPickle,
};
use vodozemac::sas::{EstablishedSas, Mac, Sas, SasBytes};
use vodozemac::{Curve25519PublicKey, DecodeError, KeyError, LibolmPickleError, PickleError};

// Errors
// ------

mod errors {
    rustler::atoms! {
        /// invalid base 64 (session creation, curve25519 key, olm/megolm decryption, inbound session, SAS verification, pickle)
        invalid_base64,
        /// invalid key length (curve25519 key, inbound session, pickle)
        invalid_key_length,
        /// invalid message type (decryption)
        invalid_message_type,
        /// missing one time key (account, olm session creation)
        missing_one_time_key,
        /// mismatched identity key (account, olm session creation)
        mismatched_identity_key,
        /// invalid MAC (olm/megolm decryption, SAS verification)
        invalid_mac,
        /// invalid MAC length (olm/megolm decryption, SAS verification)
        invalid_mac_length,
        /// invalid padding (olm/megolm decryption)
        invalid_padding,
        /// message key can't been created (olm decryption)
        missing_message_key,
        /// message gap too big (olm decryption)
        too_big_message_gap,
        /// missing version (decode)
        missing_version,
        /// invalid version (inbound session, pickle)
        invalid_version,
        /// invalid signature (inbound session, megolm decryption)
        invalid_signature,
        /// unknown message index (megolm decryption)
        unknown_message_index,
        /// at least one of the keys did not have contributory behaviour (SAS verification)
        non_contributory_key,
        /// the SAS shared secret has already been established
        already_established,
        /// decryption error (olm session creation, pickle)
        decryption,
        /// message too short (decode)
        message_too_short,
        /// protobuf error (decode)
        proto_buf_error,
        /// serialization error (pickle)
        serialization,
        /// invalid public key (pickle)
        invalid_public_key,
        /// no valid receiving or sending chain (pickle)
        invalid_session,
        /// decode error (pickle)
        decode,
        unknown,
    }
}

fn key_error_to_atom(err: KeyError) -> Atom {
    match err {
        KeyError::Base64Error(..) => errors::invalid_base64(),
        KeyError::InvalidKeyLength(..) => errors::invalid_key_length(),
        KeyError::Signature(..) => errors::invalid_signature(),
        KeyError::NonContributoryKey => errors::non_contributory_key(),
    }
}

fn olm_decryption_error_to_atom(err: OlmDecryptionError) -> Atom {
    match err {
        OlmDecryptionError::InvalidMAC(_) => errors::invalid_mac(),
        OlmDecryptionError::InvalidMACLength(..) => errors::invalid_mac_length(),
        OlmDecryptionError::InvalidPadding(_) => errors::invalid_padding(),
        OlmDecryptionError::MissingMessageKey(_) => errors::missing_message_key(),
        OlmDecryptionError::TooBigMessageGap(..) => errors::too_big_message_gap(),
    }
}

fn session_creation_error_to_atom(err: SessionCreationError) -> Atom {
    match err {
        SessionCreationError::MissingOneTimeKey(..) => errors::missing_one_time_key(),
        SessionCreationError::MismatchedIdentityKey(..) => errors::mismatched_identity_key(),
        SessionCreationError::Decryption(..) => errors::decryption(),
    }
}

fn session_key_decode_error_to_atom(err: SessionKeyDecodeError) -> Atom {
    match err {
        SessionKeyDecodeError::Version(..) => errors::invalid_version(),
        SessionKeyDecodeError::Read(..) => errors::invalid_key_length(),
        SessionKeyDecodeError::Base64(..) => errors::invalid_base64(),
        SessionKeyDecodeError::Signature(..) => errors::invalid_signature(),
        SessionKeyDecodeError::PublicKey(key_error) => key_error_to_atom(key_error),
    }
}

fn decode_error_to_atom(err: DecodeError) -> Atom {
    match err {
        DecodeError::MessageType(..) => errors::invalid_message_type(),
        DecodeError::MissingVersion => errors::missing_version(),
        DecodeError::MessageTooShort(..) => errors::message_too_short(),
        DecodeError::InvalidVersion(..) => errors::invalid_version(),
        DecodeError::InvalidKey(key_error) => key_error_to_atom(key_error),
        DecodeError::InvalidMacLength(..) => errors::invalid_mac(),
        DecodeError::Signature(..) => errors::invalid_signature(),
        DecodeError::ProtoBufError(..) => errors::proto_buf_error(),
        DecodeError::Base64(..) => errors::invalid_base64(),
    }
}

fn megolm_decryption_error_to_atom(err: MegolmDecryptionError) -> Atom {
    match err {
        MegolmDecryptionError::Signature(..) => errors::invalid_signature(),
        MegolmDecryptionError::InvalidMAC(..) => errors::invalid_mac(),
        MegolmDecryptionError::InvalidMACLength(..) => errors::invalid_mac_length(),
        MegolmDecryptionError::InvalidPadding(..) => errors::invalid_padding(),
        MegolmDecryptionError::UnknownMessageIndex(..) => errors::unknown_message_index(),
    }
}

fn pickle_error_to_atom(err: PickleError) -> Atom {
    match err {
        PickleError::Base64(..) => errors::invalid_base64(),
        PickleError::Decryption(..) => errors::decryption(),
        PickleError::Serialization(..) => errors::serialization(),
    }
}

fn libolm_pickle_error_to_atom(err: LibolmPickleError) -> Atom {
    match err {
        LibolmPickleError::MissingVersion => errors::missing_version(),
        LibolmPickleError::Version(..) => errors::invalid_version(),
        LibolmPickleError::Base64(..) => errors::invalid_base64(),
        LibolmPickleError::Decryption(..) => errors::decryption(),
        LibolmPickleError::PublicKey(..) => errors::invalid_public_key(),
        LibolmPickleError::InvalidSession => errors::invalid_session(),
        LibolmPickleError::Decode(..) => errors::decode(),
    }
}

// Utils
// -----

/// wraps a Vec<u8> and encodes to an Elixir binary.  Also blanks the vector
/// when it is deallocated, to avoid having extra copies remaining in memory.
struct VecU8Binary(Vec<u8>);

impl VecU8Binary {
    fn new(data: Vec<u8>) -> Self {
        VecU8Binary(data)
    }
}

impl From<Vec<u8>> for VecU8Binary {
    fn from(data: Vec<u8>) -> Self {
        Self::new(data)
    }
}

impl Drop for VecU8Binary {
    fn drop(&mut self) {
        self.0.fill(0);
    }
}

impl Encoder for VecU8Binary {
    fn encode<'b>(&self, env: Env<'b>) -> Term<'b> {
        let len = self.0.len();
        let mut bin = match OwnedBinary::new(len) {
            Some(bin) => bin,
            None => panic!("binary term allocation fail"),
        };
        bin.as_mut_slice()
            .write_all(self.0.as_slice())
            .expect("memory copy of vec failed");
        bin.release(env).to_term(env)
    }
}

mod session_ordering {
    rustler::atoms! {
        equal,
        better,
        worse,
        unconnected,
    }
}

#[derive(NifMap)]
struct AuthenticationStrings {
    emoji: (u8, u8, u8, u8, u8, u8, u8),
    decimal: (u16, u16, u16),
}

impl AuthenticationStrings {
    pub fn new(bytes: SasBytes) -> Self {
        let emoji_indices = bytes.emoji_indices();
        AuthenticationStrings {
            emoji: (
                emoji_indices[0],
                emoji_indices[1],
                emoji_indices[2],
                emoji_indices[3],
                emoji_indices[4],
                emoji_indices[5],
                emoji_indices[6],
            ),
            decimal: bytes.decimals(),
        }
    }
}

impl From<SasBytes> for AuthenticationStrings {
    fn from(bytes: SasBytes) -> Self {
        AuthenticationStrings::new(bytes)
    }
}

// Olm Account
// -----------

#[rustler::nif]
fn account_create() -> ResourceArc<AccountResource> {
    ResourceArc::new(AccountResource(Mutex::new(Account::new())))
}

#[rustler::nif]
fn account_ed25519_key(resource: ResourceArc<AccountResource>) -> String {
    let account = resource.0.lock().unwrap();
    account.ed25519_key().to_base64()
}

#[rustler::nif]
fn account_curve25519_key(resource: ResourceArc<AccountResource>) -> String {
    let account = resource.0.lock().unwrap();
    account.curve25519_key().to_base64()
}

#[rustler::nif]
fn account_sign(resource: ResourceArc<AccountResource>, message: &str) -> String {
    let account = resource.0.lock().unwrap();
    account.sign(message).to_base64()
}

#[rustler::nif]
fn account_max_number_of_one_time_keys(resource: ResourceArc<AccountResource>) -> usize {
    let account = resource.0.lock().unwrap();
    account.max_number_of_one_time_keys()
}

#[rustler::nif]
fn account_generate_one_time_keys(resource: ResourceArc<AccountResource>, num: usize) {
    let mut account = resource.0.lock().unwrap();
    account.generate_one_time_keys(num)
}

#[rustler::nif]
fn account_get_one_time_keys(resource: ResourceArc<AccountResource>) -> HashMap<String, String> {
    let account = resource.0.lock().unwrap();
    HashMap::from_iter(
        account
            .one_time_keys()
            .iter()
            .map(|(key_id, key)| (key_id.to_base64(), key.to_base64())),
    )
}

#[rustler::nif]
fn account_generate_fallback_key(resource: ResourceArc<AccountResource>) {
    let mut account = resource.0.lock().unwrap();
    account.generate_fallback_key()
}

#[rustler::nif]
fn account_get_fallback_key(resource: ResourceArc<AccountResource>) -> HashMap<String, String> {
    let account = resource.0.lock().unwrap();
    account
        .fallback_key()
        .iter()
        .map(|(key_id, key)| (key_id.to_base64(), key.to_base64()))
        .collect()
}

#[rustler::nif]
fn account_forget_fallback_key(resource: ResourceArc<AccountResource>) -> bool {
    let mut account = resource.0.lock().unwrap();
    account.forget_fallback_key()
}

#[rustler::nif]
fn account_mark_keys_as_published(resource: ResourceArc<AccountResource>) {
    let mut account = resource.0.lock().unwrap();
    account.mark_keys_as_published()
}

#[rustler::nif]
fn account_create_outbound_session(
    resource: ResourceArc<AccountResource>,
    identity_key_b64: &str,
    one_time_key_b64: &str,
) -> Result<ResourceArc<SessionResource>, Atom> {
    let account = resource.0.lock().unwrap();

    let identity_key =
        Curve25519PublicKey::from_base64(identity_key_b64).map_err(key_error_to_atom)?;
    let one_time_key =
        Curve25519PublicKey::from_base64(one_time_key_b64).map_err(key_error_to_atom)?;

    Ok(ResourceArc::new(SessionResource(Mutex::new(
        // FIXME: allow configuring session
        account.create_outbound_session(OlmSessionConfig::version_1(), identity_key, one_time_key),
    ))))
}

#[rustler::nif]
fn account_create_inbound_session(
    resource: ResourceArc<AccountResource>,
    identity_key_b64: &str,
    ciphertext: &str,
) -> Result<(ResourceArc<SessionResource>, VecU8Binary), Atom> {
    let mut account = resource.0.lock().unwrap();

    let identity_key =
        Curve25519PublicKey::from_base64(identity_key_b64).map_err(key_error_to_atom)?;

    let message = match OlmMessage::from_parts(0, ciphertext) {
        Ok(OlmMessage::PreKey(message)) => Ok(message),
        _ =>
        // this will never happen because we set the message type to 0
        {
            Err(errors::invalid_message_type())
        }
    }?;

    let result = account
        .create_inbound_session(identity_key, &message)
        .map_err(session_creation_error_to_atom)?;

    Ok((
        ResourceArc::new(SessionResource(Mutex::new(result.session))),
        result.plaintext.into(),
    ))
}

#[rustler::nif]
fn account_pickle(resource: ResourceArc<AccountResource>, key: Binary) -> Result<String, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let account = resource.0.lock().unwrap();
    Ok(account.pickle().encrypt(key_arr))
}

#[rustler::nif]
fn account_from_pickle(pickle: &str, key: Binary) -> Result<ResourceArc<AccountResource>, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let pickle = AccountPickle::from_encrypted(pickle, key_arr).map_err(pickle_error_to_atom)?;
    Ok(ResourceArc::new(AccountResource(Mutex::new(
        Account::from_pickle(pickle),
    ))))
}

#[rustler::nif]
fn account_from_libolm_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<AccountResource>, Atom> {
    Ok(ResourceArc::new(AccountResource(Mutex::new(
        Account::from_libolm_pickle(pickle, key.as_slice()).map_err(libolm_pickle_error_to_atom)?,
    ))))
}

// Olm Sessions
// ------------

#[rustler::nif]
fn session_id(resource: ResourceArc<SessionResource>) -> String {
    let session = resource.0.lock().unwrap();
    session.session_id()
}

#[rustler::nif]
fn session_has_received_message(resource: ResourceArc<SessionResource>) -> bool {
    let session = resource.0.lock().unwrap();
    session.has_received_message()
}

#[rustler::nif]
fn session_encrypt(resource: ResourceArc<SessionResource>, plaintext: Binary) -> (usize, String) {
    let mut session = resource.0.lock().unwrap();
    let message = session.encrypt(plaintext.as_slice());
    message.to_parts()
}

#[rustler::nif]
fn session_keys(resource: ResourceArc<SessionResource>) -> (String, String, String) {
    let session = resource.0.lock().unwrap();
    let keys = session.session_keys();
    (
        keys.identity_key.to_base64(),
        keys.base_key.to_base64(),
        keys.one_time_key.to_base64(),
    )
}

// TODO: session_config

#[rustler::nif]
fn session_decrypt(
    resource: ResourceArc<SessionResource>,
    message_type: usize,
    ciphertext: &str,
) -> Result<VecU8Binary, Atom> {
    let mut session = resource.0.lock().unwrap();
    let olm_message =
        OlmMessage::from_parts(message_type, ciphertext).map_err(decode_error_to_atom)?;

    session
        .decrypt(&olm_message)
        .map_err(olm_decryption_error_to_atom)
        .map(VecU8Binary::new)
}

#[rustler::nif]
fn session_pickle(resource: ResourceArc<SessionResource>, key: Binary) -> Result<String, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let session = resource.0.lock().unwrap();
    Ok(session.pickle().encrypt(key_arr))
}

#[rustler::nif]
fn session_from_pickle(pickle: &str, key: Binary) -> Result<ResourceArc<SessionResource>, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let pickle = SessionPickle::from_encrypted(pickle, key_arr).map_err(pickle_error_to_atom)?;
    Ok(ResourceArc::new(SessionResource(Mutex::new(
        Session::from_pickle(pickle),
    ))))
}

#[rustler::nif]
fn session_from_libolm_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<SessionResource>, Atom> {
    Ok(ResourceArc::new(SessionResource(Mutex::new(
        Session::from_libolm_pickle(pickle, key.as_slice()).map_err(libolm_pickle_error_to_atom)?,
    ))))
}

// Megolm Sessions
// ---------------

#[rustler::nif]
fn group_session_create() -> ResourceArc<GroupSessionResource> {
    ResourceArc::new(GroupSessionResource(Mutex::new(GroupSession::new(
        MegolmSessionConfig::version_1(),
    ))))
}

#[rustler::nif]
fn group_session_id(resource: ResourceArc<GroupSessionResource>) -> String {
    let session = resource.0.lock().unwrap();
    session.session_id()
}

#[rustler::nif]
fn group_session_message_index(resource: ResourceArc<GroupSessionResource>) -> u32 {
    let session = resource.0.lock().unwrap();
    session.message_index()
}

#[rustler::nif]
fn group_session_encrypt(resource: ResourceArc<GroupSessionResource>, message: Binary) -> String {
    let mut session = resource.0.lock().unwrap();
    session.encrypt(message.as_slice()).to_base64()
}

#[rustler::nif]
fn group_session_key(resource: ResourceArc<GroupSessionResource>) -> String {
    let session = resource.0.lock().unwrap();
    session.session_key().to_base64()
}

#[rustler::nif]
fn group_session_pickle(
    resource: ResourceArc<GroupSessionResource>,
    key: Binary,
) -> Result<String, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let session = resource.0.lock().unwrap();
    Ok(session.pickle().encrypt(key_arr))
}

#[rustler::nif]
fn group_session_from_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<GroupSessionResource>, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let pickle =
        GroupSessionPickle::from_encrypted(pickle, key_arr).map_err(pickle_error_to_atom)?;
    Ok(ResourceArc::new(GroupSessionResource(Mutex::new(
        GroupSession::from_pickle(pickle),
    ))))
}

#[rustler::nif]
fn group_session_from_libolm_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<GroupSessionResource>, Atom> {
    Ok(ResourceArc::new(GroupSessionResource(Mutex::new(
        GroupSession::from_libolm_pickle(pickle, key.as_slice())
            .map_err(libolm_pickle_error_to_atom)?,
    ))))
}

// Megolm Inbound Group Sessions
// -----------------------------

#[rustler::nif]
fn inbound_group_session_create(
    key: &str,
) -> Result<ResourceArc<InboundGroupSessionResource>, Atom> {
    let session_key = SessionKey::from_base64(key).map_err(session_key_decode_error_to_atom)?;
    Ok(ResourceArc::new(InboundGroupSessionResource(Mutex::new(
        InboundGroupSession::new(&session_key, MegolmSessionConfig::version_1()),
    ))))
}

#[rustler::nif]
fn inbound_group_session_import(
    key: &str,
) -> Result<ResourceArc<InboundGroupSessionResource>, Atom> {
    let session_key =
        ExportedSessionKey::from_base64(key).map_err(session_key_decode_error_to_atom)?;
    Ok(ResourceArc::new(InboundGroupSessionResource(Mutex::new(
        InboundGroupSession::import(&session_key, MegolmSessionConfig::version_1()),
    ))))
}

#[rustler::nif]
fn inbound_group_session_id(resource: ResourceArc<InboundGroupSessionResource>) -> String {
    let session = resource.0.lock().unwrap();
    session.session_id()
}

#[rustler::nif]
fn inbound_group_session_connected(
    resource1: ResourceArc<InboundGroupSessionResource>,
    resource2: ResourceArc<InboundGroupSessionResource>,
) -> bool {
    let mut session1 = resource1.0.lock().unwrap();
    let mut session2 = resource2.0.lock().unwrap();
    session1.connected(&mut *session2)
}

#[rustler::nif]
fn inbound_group_session_compare(
    resource1: ResourceArc<InboundGroupSessionResource>,
    resource2: ResourceArc<InboundGroupSessionResource>,
) -> Atom {
    let mut session1 = resource1.0.lock().unwrap();
    let mut session2 = resource2.0.lock().unwrap();
    match session1.compare(&mut *session2) {
        SessionOrdering::Equal => session_ordering::equal(),
        SessionOrdering::Better => session_ordering::better(),
        SessionOrdering::Worse => session_ordering::worse(),
        SessionOrdering::Unconnected => session_ordering::unconnected(),
    }
}

/*
#[rustler::nif]
fn inbound_group_session_merge(resource1: ResourceArc<InboundGroupSessionResource>, resource2: ResourceArc<InboundGroupSessionResource>) -> Option<ResourceArc<InboundGroupSessionResource>> {
    let mut session1 = resource1.0.lock().unwrap();
    let mut session2 = resource2.0.lock().unwrap();
    session1.merge(&mut *session2)
        .map(|session3| ResourceArc::new(InboundGroupSessionResource(Mutex::new(session3))))
}
*/

#[rustler::nif]
fn inbound_group_session_first_known_index(
    resource: ResourceArc<InboundGroupSessionResource>,
) -> u32 {
    let session = resource.0.lock().unwrap();
    session.first_known_index()
}

#[rustler::nif]
fn inbound_group_session_advance_to(
    resource: ResourceArc<InboundGroupSessionResource>,
    index: u32,
) -> bool {
    let mut session = resource.0.lock().unwrap();
    session.advance_to(index)
}

#[rustler::nif]
fn inbound_group_session_decrypt(
    resource: ResourceArc<InboundGroupSessionResource>,
    ciphertext: &str,
) -> Result<(VecU8Binary, u32), Atom> {
    let mut session = resource.0.lock().unwrap();
    let message = MegolmMessage::from_base64(ciphertext).map_err(decode_error_to_atom)?;
    let plaintext = session
        .decrypt(&message)
        .map_err(megolm_decryption_error_to_atom)?;
    Ok((plaintext.plaintext.into(), plaintext.message_index))
}

#[rustler::nif]
fn inbound_group_session_export_at(
    resource: ResourceArc<InboundGroupSessionResource>,
    index: u32,
) -> Option<String> {
    let mut session = resource.0.lock().unwrap();
    session.export_at(index).map(|export| export.to_base64())
}

#[rustler::nif]
fn inbound_group_session_export_at_first_known_index(
    resource: ResourceArc<InboundGroupSessionResource>,
) -> String {
    let session = resource.0.lock().unwrap();
    session.export_at_first_known_index().to_base64()
}

#[rustler::nif]
fn inbound_group_session_pickle(
    resource: ResourceArc<InboundGroupSessionResource>,
    key: Binary,
) -> Result<String, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let session = resource.0.lock().unwrap();
    Ok(session.pickle().encrypt(key_arr))
}

#[rustler::nif]
fn inbound_group_session_from_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<InboundGroupSessionResource>, Atom> {
    let key_arr: &[u8; 32] =
        <&[u8; 32]>::try_from(key.as_slice()).map_err(|_| errors::invalid_key_length())?;
    let pickle =
        InboundGroupSessionPickle::from_encrypted(pickle, key_arr).map_err(pickle_error_to_atom)?;
    Ok(ResourceArc::new(InboundGroupSessionResource(Mutex::new(
        InboundGroupSession::from_pickle(pickle),
    ))))
}

#[rustler::nif]
fn inbound_group_session_from_libolm_pickle(
    pickle: &str,
    key: Binary,
) -> Result<ResourceArc<InboundGroupSessionResource>, Atom> {
    Ok(ResourceArc::new(InboundGroupSessionResource(Mutex::new(
        InboundGroupSession::from_libolm_pickle(pickle, key.as_slice())
            .map_err(libolm_pickle_error_to_atom)?,
    ))))
}

// SAS
// ---

#[rustler::nif]
fn sas_create() -> (ResourceArc<SasResource>, String) {
    let sas = Sas::new();
    let public_key = sas.public_key().to_base64();
    (
        ResourceArc::new(SasResource(Mutex::new(Some(sas)))),
        public_key,
    )
}

#[rustler::nif]
fn sas_establish(
    resource: ResourceArc<SasResource>,
    key: &str,
) -> Result<ResourceArc<EstablishedSasResource>, Atom> {
    let sas = resource
        .0
        .lock()
        .unwrap()
        .take()
        .ok_or_else(errors::already_established)?;
    let established_sas = sas
        .diffie_hellman_with_raw(key)
        .map_err(key_error_to_atom)?;
    Ok(ResourceArc::new(EstablishedSasResource(established_sas)))
}

#[rustler::nif]
fn sas_generate_auth_string(
    resource: ResourceArc<EstablishedSasResource>,
    info: &str,
) -> AuthenticationStrings {
    let established_sas = &resource.0;
    established_sas.bytes(info).into()
}

#[rustler::nif]
fn sas_calculate_mac(
    resource: ResourceArc<EstablishedSasResource>,
    input: &str,
    info: &str,
) -> String {
    let established_sas = &resource.0;
    established_sas.calculate_mac(input, info).to_base64()
}

#[rustler::nif]
fn sas_calculate_mac_invalid_base64(
    resource: ResourceArc<EstablishedSasResource>,
    input: &str,
    info: &str,
) -> String {
    let established_sas = &resource.0;
    established_sas.calculate_mac_invalid_base64(input, info)
}

#[rustler::nif]
fn sas_verify_mac(
    resource: ResourceArc<EstablishedSasResource>,
    input: &str,
    info: &str,
    tag: &str,
) -> Result<(), Atom> {
    let established_sas = &resource.0;
    let mac = Mac::from_base64(tag).map_err(|_| errors::invalid_base64())?;
    established_sas
        .verify_mac(input, info, &mac)
        .map_err(|_| errors::invalid_mac())
}

#[rustler::nif]
fn sas_our_public_key(resource: ResourceArc<EstablishedSasResource>) -> String {
    let established_sas = &resource.0;
    established_sas.our_public_key().to_base64()
}

#[rustler::nif]
fn sas_their_public_key(resource: ResourceArc<EstablishedSasResource>) -> String {
    let established_sas = &resource.0;
    established_sas.their_public_key().to_base64()
}

rustler::init!(
    "Elixir.Polyjuice.Newt.Nif",
    [
        account_create,
        account_ed25519_key,
        account_curve25519_key,
        account_sign,
        account_max_number_of_one_time_keys,
        account_generate_one_time_keys,
        account_get_one_time_keys,
        account_generate_fallback_key,
        account_get_fallback_key,
        account_forget_fallback_key,
        account_mark_keys_as_published,
        account_create_outbound_session,
        account_create_inbound_session,
        account_pickle,
        account_from_pickle,
        account_from_libolm_pickle,
        session_id,
        session_has_received_message,
        session_encrypt,
        session_keys,
        session_decrypt,
        session_pickle,
        session_from_pickle,
        session_from_libolm_pickle,
        group_session_create,
        group_session_id,
        group_session_message_index,
        group_session_encrypt,
        group_session_key,
        group_session_pickle,
        group_session_from_pickle,
        group_session_from_libolm_pickle,
        inbound_group_session_create,
        inbound_group_session_import,
        inbound_group_session_id,
        inbound_group_session_connected,
        inbound_group_session_compare,
        inbound_group_session_first_known_index,
        inbound_group_session_advance_to,
        inbound_group_session_decrypt,
        inbound_group_session_export_at,
        inbound_group_session_export_at_first_known_index,
        inbound_group_session_pickle,
        inbound_group_session_from_pickle,
        inbound_group_session_from_libolm_pickle,
        sas_create,
        sas_establish,
        sas_generate_auth_string,
        sas_calculate_mac,
        sas_calculate_mac_invalid_base64,
        sas_verify_mac,
        sas_our_public_key,
        sas_their_public_key,
    ],
    load = on_load
);

struct AccountResource(Mutex<Account>);
struct SessionResource(Mutex<Session>);
struct GroupSessionResource(Mutex<GroupSession>);
struct InboundGroupSessionResource(Mutex<InboundGroupSession>);
struct SasResource(Mutex<Option<Sas>>);
struct EstablishedSasResource(EstablishedSas);

fn on_load(env: Env, _info: Term) -> bool {
    resource!(AccountResource, env);
    resource!(SessionResource, env);
    resource!(GroupSessionResource, env);
    resource!(InboundGroupSessionResource, env);
    resource!(SasResource, env);
    resource!(EstablishedSasResource, env);
    true
}
